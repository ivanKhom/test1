﻿
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class WebCamHandler : MonoBehaviour
{

    //states
    #region States
    private int _currentCamIndex = 0;
    private WebCamTexture _webCamTexture;
    private WebCamDevice _device;
    private bool _isPlaying = false;
    private int _webCamBlockSize;
    private Dictionary<Vector2,Waypoint> grid = new Dictionary<Vector2, Waypoint>();
    #endregion
    
    [SerializeField]  Color seekingColor;   
    
    [SerializeField][Tooltip("Update screen analize every X sec: ")]  float UpdateEvery = 0.5f;



    private void Awake()
    {
        _device = WebCamTexture.devices[_currentCamIndex];
        _webCamTexture = new WebCamTexture(_device.name);
        
        
        var temp = FindObjectsOfType<Waypoint>();
        _webCamBlockSize = temp.First().GridSize * 10;
        foreach (var waypoint in temp)
        {
            if (!grid.ContainsKey(waypoint.GridPoss))
            {
                grid.Add(waypoint.GridPoss,waypoint);
            }
            else
            {
                Debug.LogError("The are eqals bocks on the greed."+waypoint.GridPoss);
            }
        }
    }

    public void On_StarStopCam_Clicked()
    {
        if (_webCamTexture == null) return;
        
        if (_isPlaying)
        {
            CancelInvoke();
            _webCamTexture.Stop();
        }
        else
        {
            InvokeRepeating("AnalizePicture", 0, UpdateEvery);
            _webCamTexture.Play();
        }
        _isPlaying = !_isPlaying;
    }
    
    private void AnalizePicture()//called via string !!
    {
        
        MarkBlocksWithSearchedColor();
    }
    
    private void MarkBlocksWithSearchedColor()
    {

        FillTheBlocks(_webCamBlockSize);
        Pixel searchedPixel = new Pixel(seekingColor);
        foreach (var gridBox in grid)
        {
            gridBox.Value.isLighted = gridBox.Value.PrioratarisedPixel==searchedPixel;
        }

    }
    private void FillTheBlocks(int pixlesUnitCount)
    {
        
        
        var rowCount = _webCamTexture.width / pixlesUnitCount;
        
        for (int currentRow = 0; currentRow < rowCount; currentRow++)
        {
            
            var colomnCount = _webCamTexture.height / pixlesUnitCount;
            
            for (int currentColumn = 0; currentColumn < colomnCount; currentColumn++)
            {
                List<Color> allPixelsInBlock = new List<Color>();
                var pixelX = currentRow*pixlesUnitCount+pixlesUnitCount/2;
                var pixelY = currentColumn*pixlesUnitCount+pixlesUnitCount/2;
                allPixelsInBlock.Add(_webCamTexture.GetPixel(pixelX, pixelY));

                var blockObject = grid[new Vector2(currentRow, currentColumn)];
                blockObject.SetPixels(allPixelsInBlock);

            }
            
        }
        
    }
    
}

