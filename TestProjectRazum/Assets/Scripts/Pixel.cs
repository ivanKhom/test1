﻿using System;
using UnityEngine;


    public class Pixel
    {

        public Pixel(Color color)
        {
            PixelColor = color;
        }

        private Color _pixelColor;
        public Color PixelColor
        {
            get { return _pixelColor; }
            set
            {
                _pixelColor = value;
                Color.RGBToHSV(value, out _hue, out _satiration, out _brightness);
            }

        }
        private float _hue;
        public float Hue
        {
            get { return _hue; }
        }

        private float _satiration;
        public float Satiration
        {
            get { return _satiration; }
        }

        private float _brightness;
        public float Brightness
        {
            get { return _brightness; }
        }

        public static bool operator ==(Pixel pixel1, Pixel pixel2)
        {
            return Math.Abs(pixel1.Hue - pixel2.Hue) < 0.028f;
        }

        public static bool operator !=(Pixel pixel1, Pixel pixel2)
        {
            return Math.Abs(pixel1.Hue - pixel2.Hue) > 0.028f;
        }

        public static bool operator >(Pixel pixel1, Pixel pixel2)
        {
            return Math.Abs(pixel1.Hue - pixel2.Hue) > 0;
        }

        public static bool operator <(Pixel pixel1, Pixel pixel2)
        {
            return Math.Abs(pixel1.Hue - pixel2.Hue) < 0;
        }

        protected bool Equals(Pixel other)
        {
            return _hue.Equals(other._hue);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Pixel)obj);
        }

        public override int GetHashCode()
        {
            return _hue.GetHashCode();
        }

    }

