﻿
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public class Waypoint : MonoBehaviour {


    #region Properties
    [SerializeField] int gridSize = 10;
    public bool isLighted = false; 
    #endregion

    #region Fields
    //Todo make switch betwen capture image from camera and black color.
    private Color waypointInitialColor;
    private List<Pixel> _pixelsBlock;
    private MeshRenderer _renderer; 
    #endregion

    #region Properties
    private Color _prioratariseColor;

    public Color PrioratariseColor
    {
        get { return _prioratariseColor; }
    }

    private Pixel _prioratarisedPixel;
    public Pixel PrioratarisedPixel
    {
        get { return _prioratarisedPixel; }
    } 
    #endregion


    public void SetPixels(List<Color> colors)
    {
        _pixelsBlock = new List<Pixel>();
        foreach (var color in colors)
        {
            _pixelsBlock.Add(new Pixel(color));
        }
        CalculatePrioratariseColorAndPixel();
    }

    public List<Pixel> GetPixels()
    {
        return _pixelsBlock;
    }

    public void CalculatePrioratariseColorAndPixel()
    {
        var groupsOfEqualPixels = _pixelsBlock.GroupBy(e => e);
        Pixel maxPixle = groupsOfEqualPixels.First().First();
        int counter = 0;
        foreach (var group in groupsOfEqualPixels)
        {
            if (group.Count()>counter)
            {
                counter = group.Count();
                maxPixle = group.FirstOrDefault();
            }
        }

        _prioratariseColor = maxPixle.PixelColor;
        _prioratarisedPixel = maxPixle;

    }

    private void Start()
    {
        _renderer = transform.Find("Top").GetComponent<MeshRenderer>();
        waypointInitialColor = _renderer.material.color;
    }

    public int GridSize
    {
        get { return gridSize; }
    }

    private void Update()
    {
        UpdateBlockWithPriorColor();
    }

    public void UpdateBlockWithPriorColor()
    {
            _renderer.material.color = isLighted? Color.red: _prioratariseColor;
    }

    public Vector2Int GridPoss
    {
        get
        {
            return new Vector2Int
                (
                    Mathf.RoundToInt(transform.position.x / gridSize),
                    Mathf.RoundToInt(transform.position.z / gridSize)
                );
        }
    }
}
